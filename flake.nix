{
	description = "chpio-ts-utils";

	inputs = {
		nixpkgs.url = "github:NixOS/nixpkgs";
		flake-utils.url = "github:numtide/flake-utils";
	};

	outputs = { self, nixpkgs, flake-utils }:
		flake-utils.lib.eachDefaultSystem (system:
			let pkgs = import nixpkgs { inherit system; };
			nodejs = pkgs.nodejs-16_x;
			nodejsPkgs = pkgs.nodePackages.override {
				nodejs = nodejs;
			};
			in {
				devShell = pkgs.mkShell {
					name = "chpio-ts-utils";
					buildInputs = [
						nodejs
					];
				};
			}
		);
}
